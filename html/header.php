<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
?>
<h1 onclick="event_main()" id="Camagru">CAMAGRU</h1>
<div>
    <?php
        if (!isset($_SESSION['login']))
            echo '<span id="Connect" onclick="event_connect()"><img src="images/connect.png">CONNECT</span>
            <span id="Signup" onclick="event_signup()"><img src="images/signup.png">SIGN UP</span>';
        else
            echo '<span id="Disconnect" onclick="event_disconnect()"><img src="images/disconnect.png">DISCONNECT</span>
            <span id="User" onclick="event_user()"><img src="images/user.png">' . strtoupper($_SESSION['login']) . '</span>';
    ?>
</div>