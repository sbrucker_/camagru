<?php require('../php/email_setting.php'); ?>
<form action="javascript:user_login(login.value)">
    <h1>Change Login</h1>
    <input type="text" id="login" placeholder="New Login" required>
    <input type="submit" value="CHANGE LOGIN" >
</form>
<form action="javascript:user_email(email.value)">
    <h1>Change Email</h1>
    <input type="text" id="email" placeholder="New Email" required>
    <input type="submit" value="CHANGE EMAIL" >
</form>
<form action="javascript:user_password(password.value, password2.value)">
    <h1>Change Password</h1>
    <input type="password" id="password" placeholder="New Password" minlength="6" required>
    <input type="password" id="password2" placeholder="Repeat password" minlength="6" required>
    <input type="submit" value="CHANGE PASSWORD" >
</form>
<form>
    <h1>Email setting</h1>
    <div>
        <input type="checkbox" id="email_setting" onclick="event_email_setting()" <?= $checked; ?>>
        <label for="email_setting">Receive an email when someone comment a photo you took</label>
    </div>
</form>