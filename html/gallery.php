<div class="gallery">
    <img src="images/gallery/{{img_name}}.png">
    <div>
        <div class="comments" id="comments_{{img_name}}">
            {{comments}}
        </div>
        <?php if (session_status() === PHP_SESSION_NONE && isset($_SESSION) && !empty($_SESSION['login'])) : ?>
                <form action="javascript:add_comment(cmnt_{{img_name}}.value, {{img_name}}, cmnt_{{img_name}})">
                    <input type="text" id="cmnt_{{img_name}}" placeholder="Add a comment..." size="80" autocomplete="off" required>
                    <input type="submit" value="COMMENT" >
                </form>
        <?php endif; ?>
    </div>
</div>
<hr width="97%">