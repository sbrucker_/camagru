<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start(); 

    $query_count_img = "SELECT id FROM photo ORDER BY id DESC LIMIT 1";
    $query_insert_photo = "INSERT INTO photo SET id_user = :id_user";
    $query_get_id_current_user = "SELECT id FROM users WHERE login = :login";
    
    if (isset($_SESSION['login']) && isset($_POST) && !empty($_POST['img_base_64']) && !empty($_POST['img_name']))
    {
        require_once("db_connect.php");
        $pdo = db_connect();

        $stmt = $pdo->prepare($query_get_id_current_user);
        $stmt->execute(array('login' => $_SESSION['login']));
        $id_current = $stmt->fetch()['id'];

        $stmt = $pdo->prepare($query_insert_photo);
        $stmt->execute(array('id_user' => $id_current));

        $stmt = $pdo->prepare($query_count_img);
        $stmt->execute();
        $name = $stmt->fetch()['id'];

        $img = $_POST['img_base_64'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $fileData = base64_decode($img);
        $fileName = '../images/gallery/' . $name . '.png';
        file_put_contents($fileName, $fileData);

        $destination = imagecreatefrompng($fileName);
        imageflip($destination, IMG_FLIP_HORIZONTAL);
        $source = imagecreatefrompng('../images/png/' . $_POST['img_name'] . '.png');
        $largeur_source = imagesx($source);
        $hauteur_source = imagesy($source);
        imagecopy($destination, $source, 0, 0, 0, 0, $largeur_source, $hauteur_source);
        imagepng($destination, $fileName);

    }
    require("side.php");
?>