<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();

    require_once("db_connect.php");

    if (isset($_SESSION['login']))
    {
        $query_get_user_checkbox = "SELECT email_on_comment FROM users WHERE login = :login";
    
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_get_user_checkbox);
        $stmt->execute(array('login' => $_SESSION['login']));
        if ($stmt->fetch()['email_on_comment'] === true)
            $checked = 'checked';
        else
            $checked = '';

        if ($checked === '')
            $query_update_user_checkbox = "UPDATE users SET email_on_comment = true WHERE login = :login";
        else
            $query_update_user_checkbox = "UPDATE users SET email_on_comment = false WHERE login = :login";

        $stmt = $pdo->prepare($query_update_user_checkbox);
        $stmt->execute(array('login' => $_SESSION['login']));
    }

?>