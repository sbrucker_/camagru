<?php
    if (isset($_POST) && !empty($_POST['page']))    
        $query_get_img = "SELECT id FROM photo ORDER BY id DESC LIMIT 6 OFFSET " . (($_POST['page'] - 1) * 5);
    else
        $query_get_img = "SELECT id FROM photo ORDER BY id DESC LIMIT 6";
    
    require_once("db_connect.php");
    $pdo = db_connect();
    $stmt = $pdo->prepare($query_get_img);
    $stmt->execute();
    if ($stmt->rowCount() > 0)
    {
        if (isset($_POST['page']) && $_POST['page'] !== 1)
            require_once("show_gallery.php");
        if ($stmt->rowCount() === 6)
        {
            $str = file_get_contents("/Users/sbrucker/Projects/Camagru/html/show_more.html");
            if (isset($_POST) && !empty($_POST['page']))                
                echo str_replace("{{page}}", $_POST['page'] + 1, $str);
            else
                echo str_replace("{{page}}", '2', $str);
        }
    }
?>