<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
    require('alert.php');
    require_once('check.php');    

    $query_check_existing_user = "SELECT count(1) FROM users WHERE login = :login";
    $query_insert_user = "UPDATE users SET login = :login WHERE email = :email";

    if (isset($_SESSION['login']) && isset($_POST) && !empty($_POST['login']) && check_login($_POST['login']))
    {
        $_POST['login'] = htmlspecialchars($_POST['login']);
        require("db_connect.php");
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_check_existing_user);
        $stmt->execute(array('login' => $_POST['login']));
        if ($stmt->fetchColumn() === 0)
        {
            $stmt = $pdo->prepare($query_insert_user);
            $stmt->execute(array('login' => $_POST['login'], 'email' => $_SESSION['email']));
            $_SESSION['login'] = $_POST['login'];
            create_alert("Done !");
        }
        else
            create_alert("This login is already used.");
    }
    else
        create_alert("Please fill each field.");
?>