<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();   
    require("alert.php");

    $query_check_existing_user = "SELECT login, email FROM users WHERE login = :login AND password = :password AND validate = 1";

    if (!isset($_SESSION['login']) && isset($_POST) && !empty($_POST['login']) && !empty($_POST['password']))
    {
        require("db_connect.php");
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_check_existing_user);
        $stmt->execute(array('login' => $_POST['login'], 'password' => hash('whirlpool', $_POST['password'])));
        $user = $stmt->fetch();
        if (empty($user))
            create_alert("Bad login or password. Don't forget to confirm your registration with the link send in your mail.");
        else
        {
            $_SESSION['login'] = $user['login'];
            $_SESSION['email'] = $user['email'];
        }
    }
?>