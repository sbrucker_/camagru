<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();

    if (isset($_SESSION) && !empty($_SESSION['login']))
    {
        $query_get_img = "SELECT id FROM photo WHERE id_user = :id_user ORDER BY id DESC";
        $query_get_id_current_user = "SELECT id FROM users WHERE login = :login";

        require_once("db_connect.php");
        $pdo = db_connect();

        $stmt = $pdo->prepare($query_get_id_current_user);
        $stmt->execute(array('login' => $_SESSION['login']));
        $id_current = $stmt->fetch()['id'];

        $stmt = $pdo->prepare($query_get_img);
        $stmt->execute(array('id_user' => $id_current));
        
        while ($row = $stmt->fetch())
        {
            $html = file_get_contents("../html/gallery_side.html");
            $html = str_replace("{{img_name}}", $row['id'], $html);
            echo $html;            
        }
    }
?>