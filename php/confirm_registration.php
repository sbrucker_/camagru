<?php
    require("php/alert.php");

    $query_get_user = "SELECT email, timestamp, validate FROM users WHERE email = :email";
    $query_validate_user = "UPDATE users SET validate = true WHERE email = :email";

    if (!isset($_SESSION['login']) && isset($_GET) && !empty($_GET['email']) && !empty($_GET['key']))
    {
        require("db_connect.php");
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_get_user);
        $stmt->execute(array('email' => $_GET['email']));
        $user = $stmt->fetch();
        if (empty($user))
            create_alert_from_root("There was an error in the confirmation of your registration. Please try again.");
        else if ($user['validate'] === 1)
            create_alert_from_root("This email has already been confirmed.");
        else
        {
            $key = md5($_GET['email'] . $user['timestamp']);
            if ($key === $_GET['key'])
            {
                $stmt = $pdo->prepare($query_validate_user);
                $stmt->execute(array('email' => $_GET['email']));
                create_alert_from_root("Your registration is confirmed ! You can now sign in.");
            }
            else
                create_alert_from_root("There was an error in the confirmation of your registration. Please try again....");
        }
    }
?>