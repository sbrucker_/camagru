<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
    require('alert.php');
    require_once('check.php');    

    $query_insert_user = "UPDATE users SET password = :password WHERE login = :login";

    if (isset($_SESSION['login']) && isset($_POST) && !empty($_POST['password']) && !empty($_POST['password2']))
    {
        if ($_POST['password'] == $_POST['password2'] && check_password($_POST['password']))
        {
            require("db_connect.php");
            $pdo = db_connect();
            $stmt = $pdo->prepare($query_insert_user);
            $stmt->execute(array('login' => $_SESSION['login'], 'password' => hash('whirlpool', $_POST['password'])));
            create_alert("Done !");
        }
        else
            create_alert("Both passwords are not matching.");
    }
    else
        create_alert("Please fill each field.");
?>