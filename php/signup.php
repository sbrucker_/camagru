<?php
    require('send_mail.php');
    require('alert.php');
    require_once('check.php');
    
    $query_check_existing_user = "SELECT count(1) FROM users WHERE login = :login OR email = :email";
    $query_insert_user = "INSERT INTO users (login, email, password, timestamp, validate, email_on_comment) VALUES (:login, :email, :password, :timestamp, false, true)";

    if (!isset($_SESSION['login']) && isset($_POST) && !empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['password2']) && !empty($_POST['email']))
    {
        if ($_POST['password'] === $_POST['password2'] && check_password($_POST['password']) && check_login($_POST['login']) && check_email($_POST['email']))
        {
            $_POST['login'] = htmlspecialchars($_POST['login']);
            require("db_connect.php");
            $pdo = db_connect();
            $stmt = $pdo->prepare($query_check_existing_user);
            $stmt->execute(array('login' => $_POST['login'], 'email' => $_POST['email']));
            if ($stmt->fetchColumn() === 0)
            {
                $date_obj = new DateTime();
                $date = $date_obj->getTimeStamp();
                $stmt = $pdo->prepare($query_insert_user);
                $stmt->execute(array('login' => $_POST['login'], 'email' => $_POST['email'], 'password' => hash('whirlpool', $_POST['password']), 'timestamp' => $date));                
                $hash = md5($_POST['email'] . $date);
                send_signup_mail($_POST['email'], $hash);
                create_alert("Please check your emails in order to complete your registration.");
            }
            else
                create_alert("This login or this email already exists.");
        }
        else
            create_alert("Both passwords are not matching.");
    }
    else
        create_alert("Please fill each field.");
?>