<?php
    function check_email($email)
    {
        return (filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    function check_password($pass)
    {
        return (strlen($pass) >= 6);
    }

    function check_login($login)
    {
        return (strlen($login) >= 4);
    }
?>