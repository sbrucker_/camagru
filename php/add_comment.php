<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
    require('alert.php');
    require('send_mail.php');

    $query_get_user_id = "SELECT id FROM users WHERE login = :login";
    $query_check_existing_photo = "SELECT * FROM photo WHERE id = :id";
    $query_insert_comment = "INSERT INTO comments (id_photo, id_user, comment) VALUES (:id_photo, :id_user, :comment)";
    $query_get_cmnts = "SELECT comment, id_user FROM comments WHERE id_photo = :id ORDER BY id DESC";
    $query_get_user = "SELECT login FROM users WHERE id = :id_user";
    $query_get_email_photo = "SELECT users.email FROM users INNER JOIN photo ON photo.id_user = users.id WHERE photo.id = :photo_id";
    
    require("db_connect.php");
    $pdo = db_connect();
    if (isset($_SESSION['login']) && isset($_POST) && !empty($_POST['cmnt']) && !empty($_POST['id']))
    {
        $_POST['cmnt'] = htmlspecialchars($_POST['cmnt']);

        $stmt = $pdo->prepare($query_get_user_id);
        $stmt->execute(array('login' => $_SESSION['login']));
        $id = $stmt->fetch()['id'];

        $stmt = $pdo->prepare($query_get_email_photo);
        $stmt->execute(array('photo_id' => $_POST['id']));
        $email = $stmt->fetch()['email'];

        $stmt = $pdo->prepare($query_check_existing_photo);
        $stmt->execute(array('id' => $_POST['id']));
        if ($stmt->rowCount() > 0)
        {
            $stmt = $pdo->prepare($query_insert_comment);
            $stmt->execute(array('id_photo' => $_POST['id'], 'id_user' => $id, 'comment' => $_POST['cmnt']));
            send_comment_mail($email);
        }
    }
    $stmt = $pdo->prepare($query_get_cmnts);
    $stmt->execute(array('id' => $_POST['id']));
    while ($row = $stmt->fetch())
    {
        $stmt2 = $pdo->prepare($query_get_user);
        $stmt2->execute(array('id_user' => $row['id_user']));
        $row2 = $stmt2->fetch();
        echo '<p><bold>' . $row2['login'] . ' : </bold>' . $row['comment'] . '</p>';
    }
?>