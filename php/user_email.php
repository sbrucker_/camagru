<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
    require('alert.php');
    require_once('check.php');

    $query_check_existing_user = "SELECT count(1) FROM users WHERE email = :email";
    $query_insert_user = "UPDATE users SET email = :email WHERE login = :login";

    if (isset($_SESSION['login']) && isset($_POST) && !empty($_POST['email']) && check_email($_POST['email']))
    {
        require("db_connect.php");
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_check_existing_user);
        $stmt->execute(array('email' => $_POST['email']));
        if ($stmt->fetchColumn() === 0)
        {
            $stmt = $pdo->prepare($query_insert_user);
            $stmt->execute(array('email' => $_POST['email'], 'login' => $_SESSION['login']));
            $_SESSION['email'] = $_POST['email'];
            create_alert("Done !");
        }
        else
            create_alert("This email is already used.");
    }
    else
        create_alert("Please fill each field.");
?>