<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();

    $query_get_cmnts = "SELECT comment, id_user FROM comments WHERE id_photo = :id ORDER BY id DESC";
    $query_get_user = "SELECT login FROM users WHERE id = :id_user";

    if (isset($_POST) && !empty($_POST['page']))
        $query_get_img = "SELECT id FROM photo ORDER BY id DESC LIMIT 5 OFFSET " . (($_POST['page'] - 1) * 5);
    else
        $query_get_img = "SELECT id FROM photo ORDER BY id DESC LIMIT 5";

    require_once("db_connect.php");
    $pdo = db_connect();
    
    $stmt = $pdo->prepare($query_get_img);
    $stmt->execute();
    while ($row = $stmt->fetch())
    {
        $html = file_get_contents("/home/vagrant/Camagru/html/gallery.php");
        $html = str_replace("{{img_name}}", $row['id'], $html);

        $stmt2 = $pdo->prepare($query_get_cmnts);
        $stmt2->execute(array('id' => $row['id']));

        $cmnt = "";
        while ($row2 = $stmt2->fetch())
        {
            $stmt3 = $pdo->prepare($query_get_user);
            $stmt3->execute(array('id_user' => $row2['id_user']));
            $row3 = $stmt3->fetch();
            $cmnt .= '<p><bold>' . $row3['login'] . ' : </bold>' . $row2['comment'] . '</p>';
        }
        $html = str_replace("{{comments}}", $cmnt, $html);
        echo $html;
        
    }

    require_once("show_more.php");
?>