<?php
    require('alert.php');
    require('send_mail.php');

    $query_is_email = "SELECT * FROM users where email = :email";
    $query_insert_password = "UPDATE users SET password = :password WHERE email = :email";

    if (!isset($_SESSION['login']) && isset($_POST) && !empty($_POST['email']))
    {
        require("db_connect.php");
        $pdo = db_connect();

        $letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ!@#$%&?";
        $new_pw = "0123456789";
        for ($i = 0; $i < 10; ++$i)
            $new_pw [$i]= $letters[random_int(0, strlen($letters))];
        
        $stmt = $pdo->prepare($query_is_email);
        $stmt->execute(array('email' => $_POST['email']));
        if ($stmt->fetchColumn() > 0)
        {
            $stmt = $pdo->prepare($query_insert_password);
            $stmt->execute(array('email' => $_POST['email'], 'password' => hash('whirlpool', $new_pw)));
            send_password_mail($_POST['email'], $new_pw);
            create_alert("Your new password has been sent into your email box.");
        }
        else
            create_alert("This email doesn't exist.");
    }
    else
        create_alert("Please fill each field.");

?>