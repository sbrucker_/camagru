<?php
    function send_mail($to, $subject, $txt)
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: sbrucker@camagru.42.fr";

        mail($to, $subject, $txt, $headers);
    }

    function send_signup_mail($to, $hash)
    {
        $txt = file_get_contents("../html/mail_signup.html");
        $txt = str_replace('{{email}}', $to, $txt);
        $txt = str_replace('{{key}}', $hash, $txt);
        send_mail($to, 'Confirm your registration', $txt);
    }

    function send_password_mail($to, $new_pw)
    {
        $txt = file_get_contents("../html/mail_password.html");
        $txt = str_replace('{{password}}', $new_pw, $txt);
        send_mail($to, 'Password reset', $txt);
    }

    function send_comment_mail($to)
    {
        $txt = file_get_contents("../html/mail_comment.html");
        send_mail($to, 'New comment on your photo', $txt);
    }
?>