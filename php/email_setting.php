<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();

    require_once("db_connect.php");

    $query_get_user_checkbox = "SELECT email_on_comment FROM users WHERE login = :login";

    if (isset($_SESSION['login']))
    {
        $pdo = db_connect();
        $stmt = $pdo->prepare($query_get_user_checkbox);
        $stmt->execute(array('login' => $_SESSION['login']));
        if ($stmt->fetch()['email_on_comment'] === true)
            $checked = 'checked';
        else
            $checked = '';
    }
    else
    $checked = '';
?>