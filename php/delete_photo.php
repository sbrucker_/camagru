<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();

    $query_delete_photo = "DELETE FROM photo WHERE id = :id AND id_user = :id_user";
    $query_delete_comments = "DELETE FROM comments WHERE id_photo = :id_photo";
    $query_get_id_current_user = "SELECT id FROM users WHERE login = :login";    

    if (isset($_SESSION) && isset($_POST) && !empty($_POST['id']) && !empty($_SESSION['login']))
    {
        require_once("db_connect.php");
        $pdo = db_connect();

        $stmt = $pdo->prepare($query_get_id_current_user);
        $stmt->execute(array('login' => $_SESSION['login']));
        $id_user = $stmt->fetch()['id'];

        $stmt = $pdo->prepare($query_delete_comments);
        $stmt->execute(array('id_photo' => $_POST['id']));

        $stmt = $pdo->prepare($query_delete_photo);
        $stmt->execute(array('id' => $_POST['id'], 'id_user' => $id_user));
        unlink('../images/gallery/' . $_POST['id'] . '.png');
    }
    require("side.php");
?>