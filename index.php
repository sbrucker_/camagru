<?php
    if (session_status() === PHP_SESSION_NONE)
        session_start();
    require("php/confirm_registration.php");
?>
<html id="html">
    <?php include 'html/head.html'; ?>
    <body id="body">
        <header id="header">
            <?php include 'html/header.php'; ?>
        </header>
        <section id="section">
            <div id="main">
                <?php require("php/show_gallery.php"); ?>
            </div>
        </section>
        <?php include 'html/footer.html'; ?>
        <?php include 'html/photo.php'; ?>
    </body>
</html>