var active_cam = false;
var img_vid = null;

function event_img_none()
{
    var img = document.getElementById("img_video");
    if (img !== null)
        img.parentNode.removeChild(img);
}

function add_img_video(name)
{
    event_img_none();
    var img = document.createElement("img");
    img.src = "images/png/" + name + ".png";
    img.style.position = "absolute";
    img.id = "img_video";
    var main = document.getElementById("main");
    main.appendChild(img);
    img_vid = name;
}

function activate_camera()
{
    var video = document.querySelector("#videoElement");
    
    if (navigator.mediaDevices.getUserMedia) {       
        navigator.mediaDevices.getUserMedia({video: true})
    .then(function(stream) {
        video.srcObject = stream;
    })
    .catch(function(err0r) {
        console.log("Something went wrong!");
    });
    }
    active_cam = true;
}

function take_snap()
{
    if (active_cam === false)
    {
        alert("You must activate your camera.");
        return ;
    }
    if (img_vid === null)
    {
        alert("You must select an image.");
        return ;
    }
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('videoElement');

    context.drawImage(video, 0, 0, 800, 600);

    var data_URL = canvas.toDataURL();

    event_manager("php/save_snap.php", "side", "img_name=" + img_vid + "&img_base_64=" + data_URL);   
}

function event_file_upload()
{
    data = document.getElementById("file_upload").files[0];
    reader = new FileReader();
    reader.readAsDataURL(data);
    reader.addEventListener("load", function () {
        event_manager("php/save_snap.php", "side", "img_name=" + img_vid + "&img_base_64=" + reader.result);
      }, false);
    
    //alert(1);
    /*fileName = data.name;
    mimeType = data.type;
    xhttp = new XMLHttpRequest;
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
            document.getElementById("main").innerHTML += this.responseText;
    };
    xhttp.open('POST', 'php/upload_file.php', true);
    xhttp.setRequestHeader('Content-Type', mimeType);
    xhttp.setRequestHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');
    xhttp.send(data);*/
}