function event_manager(filename, id_to_replace, data)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200 && id_to_replace !== null)
            document.getElementById(id_to_replace).innerHTML = this.responseText;
            var elem = document.getElementById("script");
            if (elem !== null)
                eval(elem.innerHTML);
    };
    xhttp.open("POST", filename, true);
    if (data === null)
        xhttp.send();
    else
    {
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(data);
    }
}

function event_manager_add(filename, id_to_replace, data)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200 && id_to_replace !== null)
            document.getElementById(id_to_replace).innerHTML += this.responseText;
    };
    xhttp.open("POST", filename, true);
    if (data === null)
        xhttp.send();
    else
    {
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(data);
    }
}

function event_connect()
{
    event_manager("html/connect.html", "main", null);
}

function event_signup()
{
    event_manager("html/signup.html", "main", null);
}

function signup(login, password, password2, email)
{
    var data = "login=" + login + "&password=" + password + "&password2=" + password2 + "&email=" + email;
    event_manager_add("php/signup.php", "main", data);
}

function connect(login, password)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
            document.getElementById('main').innerHTML += this.responseText;   
            event_manager("php/show_gallery.php", "main", null);
            event_manager_add("html/photo.php", "body", null);
            event_manager("html/header.php", 'header', null);     
    };
    xhttp.open("POST", "php/connect.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("login=" + login + "&password=" + password);
}

function event_disconnect()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
            document.getElementById('main').innerHTML += this.responseText;
            event_manager("php/show_gallery.php", "main", null);
            event_manager("html/header.php", 'header', null);        
        };
    xhttp.open("POST", "php/disconnect.php", true);
    xhttp.send();

    var elem = document.getElementById('photo');
    if (elem !== null)
        elem.parentNode.removeChild(elem);
    elem = document.getElementById('side');
    if (elem !== null)
        elem.parentNode.removeChild(elem);
}

function event_user()
{
    event_manager("html/user.php", 'main', null);
    var elem = document.getElementById('side');
    if (elem !== null)
        elem.parentNode.removeChild(elem);
}

function user_login(login)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
            document.getElementById('main').innerHTML += this.responseText;
            event_manager("html/header.php", 'header', null);
        };
    xhttp.open("POST", "php/user_login.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("login=" + login);
}

function user_email(email)
{
    var data = "email=" + email;
    event_manager_add("php/user_email.php", "main", data);
}

function user_password(password, password2)
{
    var data = "password=" + password + "&password2=" + password2;
    event_manager_add("php/user_password.php", "main", data);
}

function event_forget_password()
{
    event_manager("html/forget_password.html", "main", null);
}

function forget_password(email)
{
    var data = "email=" + email;
    event_manager_add("php/forget_password.php", 'main', data);
}

function event_main()
{
    var data = "page=1";
    event_manager("php/show_gallery.php", "main", data);
    event_manager_add("html/photo.php", "body", null);
    var elem = document.getElementById('side');
    if (elem !== null)
        elem.parentNode.removeChild(elem);
    var elem = document.getElementById('photo');
    if (elem !== null)  
        elem.parentNode.removeChild(elem);
}

function event_photo()
{
    event_manager("html/webcam.html", "main", null);
    event_manager_add("html/side.html", "section", null);
    event_manager("php/side.php", "side", null);
    var elem = document.getElementById('photo');
    if (elem !== null)  
        elem.parentNode.removeChild(elem);
}

function add_comment(cmnt, id, elem)
{
    var data = "cmnt=" + cmnt + "&id=" + id;
    event_manager("php/add_comment.php", "comments_" + id, data);
    if (elem !== null)  
        elem.value = '';
}

function event_show_more(page)
{
    var data = "page=" + page;
    var elem = document.getElementById('show_more_' + page);
    if (elem !== null)  
        elem.parentNode.removeChild(elem);
    event_manager_add("php/show_more.php", "main", data);

}

function delete_img(id)
{
    if (confirm("Do you want to delete this photo ?"))
    {
        var data = "id=" + id;
        event_manager("php/delete_photo.php", "side", data);
    }
}

function event_email_setting()
{
    event_manager("php/email_setting_change.php", null, null);
}