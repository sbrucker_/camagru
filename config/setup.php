<?php
    require("../php/db_connect.php");
    $pdo = db_connect();
    $pdo->exec("CREATE TABLE `comments` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `id_photo` int(11) NOT NULL,
            `id_user` int(11) NOT NULL,
            `comment` text NOT NULL,
            PRIMARY KEY (`id`)
            )");
    $pdo->exec("CREATE TABLE `photo` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `id_user` int(11) NOT NULL,
            PRIMARY KEY (`id`)
            )");
    $pdo->exec("CREATE TABLE `users` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `login` text NOT NULL,
            `email` text NOT NULL,
            `password` text NOT NULL,
            `timestamp` int(11) NOT NULL,
            `validate` tinyint(1) NOT NULL,
            `email_on_comment` tinyint(1) NOT NULL,
            PRIMARY KEY (`id`)
            )");
?>